﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace doWhileWithUserInput
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter an integer (whole number) less than 20:");
            var index = int.Parse(Console.ReadLine());

            var count = 20;
            Console.WriteLine("");

            do
            {
                var a = index + 1;
                Console.WriteLine($"This is line number {a}");
                index++;
            } while (index < count);
        }
    }
}
